A collection of scripts for QlikView and Qlik Sense apps.

# Extract QVDs in -prj
The goal of the files in this folder is to extract all data from a QVW and store them as QVDs in the -prj folder.
You will need to create a QVW file, and paste in the script the content of the file "Extract DataModel and store as QVDs in prj.qvs"
and name this QVW file "Extract DataModel and store as QVDs in prj.qvw"
Then update the path in "ReloadQlikDataModelExtractor.bat" and run it.

# about-script-tab.qvs
About section to add in QVW script.

# Generate random points.qvs
Script that generates a list of coordinates to visualise a random cloud of points.

# Permutations.qvs
Generates a list of all possible combinations for weeks in the following format:
A-A, A-B, ..., A-Z, B-B, B-c, ..., B-Z, ..., Y-Y, Y-Z, Z-Z

# QVD Monitoring
This tool can be used to check the number of records, number of segments, or any other metric that is relevant to the source file per date
to ensure that there is no gap in the data sets.

You can edit the script to target a specific set of QVDs.
You will have to indicate the field name for the reference date and the which metric you want to use to monitor the QVDs.

# Scan QVDs size and list of fields
This script loads recursively information about QVDs (size, creation date, list of fields, etc.).
It also generates a QVD file with a list of QVDs that contain 0 or only few records (to highlight potential missing data).

# Template Extract data to QVD.qvs
Template for data extraction, storing in QVDs.

# Template Transform QVD.qvs
Template for data transformation, from QVD to QVD.
