REM This script scans QVW files in a predefined directory,
REM and stores into QVDs in a -prj/QVD folder all the tables in the data model.
REM You can edit the variables below to scan a specific QVW or run through all QVWs in a directory and subdirectory.
@echo off
REM Edit the source path here - temporarily assign the following path to an available Drive letter (to avoid UNC path error)
pushd \\MyQlikViewFolder
REM
REM For each file in the directory and subdirectory indicated below, and looking like *.qvw*
FOR /R "MyProjectFolder\" %%F in (*.qvw) do (
REM Generate a QVS file with a binary load on the first row
@echo Binary [%%F];> "myPath\Extract QVDs in -prj\BinaryLoad.qvs"
REM Generate a variable on row 2
@echo LET vFile = '%%F';>> "myPath\Extract QVDs in -prj\BinaryLoad.qvs"
REM Reload the QVW Tool to extract the QVDs from the Data model of the QVW app targeted
"C:\Program Files\QlikView\Qv.exe" /r "myPath\Extract QVDs in -prj\Extract DataModel and store as QVDs in prj.qvw"
)
REM Unmount the temporary Drive letter
popd
pause
